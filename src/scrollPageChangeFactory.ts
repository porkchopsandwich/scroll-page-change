import { addChangeListenerFactory } from "./addChangeListenerFactory";
import { contentHeightGetterFactory } from "./contentHeightGetterFactory";
import { currentOffsetGetterFactory } from "./currentOffsetGetterFactory";
import {
    ContentHeightGetter,
    CurrentOffsetGetter,
    IScrollPageChange,
    IScrollPageChangeState,
    ScrollChangeListener,
    ViewportHeightGetter,
} from "./interfaces";
import { onScrollFactory } from "./onScrollFactory";
import { removeChangeListenerFactory } from "./removeChangeListenerFactory";
import { updateDimensionsFactory } from "./updateDimensionsFactory";
import { viewportHeightGetterFactory } from "./viewportHeightGetterFactory";

export const scrollPageChangeFactory = (
    ctx: EventTarget = window,
    contentHeightGetter: ContentHeightGetter = contentHeightGetterFactory(),
    currentOffsetGetter: CurrentOffsetGetter = currentOffsetGetterFactory(),
    viewportHeightGetter: ViewportHeightGetter = viewportHeightGetterFactory(),
): IScrollPageChange => {
    const state: IScrollPageChangeState = {
        viewportHeight: 0,
        contentHeight: 0,
        pageCount: 0,
        currentPage: undefined,
    };

    const scrollChangeListeners: ScrollChangeListener[] = [];
    const pageChangeListeners: ScrollChangeListener[] = [];

    const updateDimensions = updateDimensionsFactory(viewportHeightGetter, contentHeightGetter, state);

    const onScroll = onScrollFactory(currentOffsetGetter, state, scrollChangeListeners, pageChangeListeners);

    ctx.addEventListener("resize", updateDimensions);

    updateDimensions();

    ctx.addEventListener("scroll", onScroll);

    // Listener Removers

    const removeScrollChangeListener = removeChangeListenerFactory(scrollChangeListeners);

    const removePageChangeListener = removeChangeListenerFactory(pageChangeListeners);

    // Listener Adders

    const addScrollChangeListener = addChangeListenerFactory(
        scrollChangeListeners,
        onScroll,
        removeScrollChangeListener,
    );

    const addPageChangeListener = addChangeListenerFactory(pageChangeListeners, onScroll, removePageChangeListener);

    const getPageCount = (): number => {
        return state.pageCount;
    };

    const getState = (): Readonly<IScrollPageChangeState> => {
        return { ...state };
    };

    return {
        addScrollChangeListener,
        removeScrollChangeListener,
        addPageChangeListener,
        removePageChangeListener,
        getPageCount,
        updateDimensions,
        getState,
    };
};
