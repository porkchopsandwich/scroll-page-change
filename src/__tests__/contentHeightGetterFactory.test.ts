/* tslint:disable:no-void-expression */

import { contentHeightGetterFactory } from "../contentHeightGetterFactory";

test("Testing an empty document.", () => {
    const contentHeightGetter = contentHeightGetterFactory();
    expect(contentHeightGetter()).toBeGreaterThanOrEqual(0);
});
