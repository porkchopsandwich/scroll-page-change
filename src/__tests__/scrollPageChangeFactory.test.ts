/* tslint:disable:no-void-expression */

import * as lib from "../onScrollFactory";
import { scrollPageChangeFactory } from "../scrollPageChangeFactory";

test("Generates On Scroll once.", () => {

    // Mock the onScroll factory
    const mocked = jest.spyOn(lib, "onScrollFactory").mockImplementation(() => {
        return (): void => {
            // Nothing
        };
    });

    const scrollPageChange = scrollPageChangeFactory();

    // Called once during start up
    expect(mocked).toBeCalledTimes(1);

    scrollPageChange.updateDimensions();
    scrollPageChange.addScrollChangeListener(() => {
        // Listening
    });

    // Still only called once
    expect(mocked).toBeCalledTimes(1);
});
