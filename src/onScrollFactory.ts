import {
    CurrentOffsetGetter,
    IOnScrollChangeEvent,
    IScrollPageChangeState,
    OnScroll,
    ScrollChangeListener,
} from "./interfaces";

export const onScrollFactory = (
    currentOffsetGetter: CurrentOffsetGetter,
    state: IScrollPageChangeState,
    scrollChangeListeners: ScrollChangeListener[],
    pageChangeListeners: ScrollChangeListener[],
): OnScroll => {
    return (): void => {
        const topOffset = Math.min(currentOffsetGetter(), state.contentHeight - state.viewportHeight);
        const bottomOffset = topOffset + state.viewportHeight;

        const divider = 2;
        const topFraction = topOffset / state.contentHeight;
        const bottomFraction = bottomOffset / state.contentHeight;
        const middleFraction = (topFraction + bottomFraction) / divider;
        const nextPage = topFraction * state.pageCount;
        const progressDenominator = state.contentHeight - (bottomOffset - topOffset);
        const relativeProgress = progressDenominator > 0 ? topOffset / progressDenominator : 0;

        const event: IOnScrollChangeEvent = {
            contentHeight: state.contentHeight,
            viewportHeight: state.viewportHeight,
            topOffset,
            bottomOffset,
            pageCount: state.pageCount,
            topFraction,
            bottomFraction,
            middleFraction,
            currentPage: nextPage,
            relativeProgress,
        };

        scrollChangeListeners.forEach((listener) => {
            listener(event);
        });

        if (nextPage !== state.currentPage) {
            pageChangeListeners.forEach((listener) => {
                listener(event);
            });
            state.currentPage = nextPage;
        }
    };
};
