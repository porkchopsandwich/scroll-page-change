export const waitForNextFrame = (): Promise<void> => {
    return new Promise<void>(
        (resolve): void => {
            requestAnimationFrame(() => {
                resolve();
            });
        },
    );
};

const ready = (handler: () => any, context: Document = document): void => {
    if (context.body instanceof Element && /complete|loaded|interactive/.test(context.readyState)) {
        setTimeout(handler, 0);
    } else {
        context.addEventListener("DOMContentLoaded", handler, false);
    }
};

export const waitForReady = (): Promise<void> => {
    return new Promise<void>(
        (resolve): void => {
            ready(resolve);
        },
    );
};

export const waitForTime = (milliseconds = 0): Promise<void> => {
    return new Promise<void>(
        (resolve): void => {
            setTimeout(() => {
                resolve();
            }, milliseconds);
        },
    );
};
