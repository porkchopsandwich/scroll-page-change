import { ContentHeightGetter } from "./interfaces";

export const contentHeightGetterFactory = (): ContentHeightGetter => {
    return (): number => {
        return document.body.offsetHeight;
    };
};
