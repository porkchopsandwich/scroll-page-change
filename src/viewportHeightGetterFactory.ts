import { ViewportHeightGetter } from "./interfaces";

export const viewportHeightGetterFactory = (): ViewportHeightGetter => {
    return (): number => {
        return window.innerHeight;
    };
};
