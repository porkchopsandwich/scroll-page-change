import {
    OnScroll,
    ScrollChangeAddListener,
    ScrollChangeListener,
    ScrollChangeRemoveListener,
    ScrollChangeUnsubscriber,
} from "./interfaces";

export const addChangeListenerFactory = (
    listeners: ScrollChangeListener[],
    onScroll: OnScroll,
    removeListener: ScrollChangeRemoveListener,
): ScrollChangeAddListener => {
    return (listener: ScrollChangeListener): ScrollChangeUnsubscriber => {
        listeners.push(listener);

        onScroll();

        return (): void => {
            removeListener(listener);
        };
    };
};
