import { ScrollChangeListener, ScrollChangeRemoveListener } from "./interfaces";

export const removeChangeListenerFactory = (listeners: ScrollChangeListener[]): ScrollChangeRemoveListener => {
    return (listener: ScrollChangeListener): void => {
        let index = -1;
        do {
            index = listeners.indexOf(listener);
            if (index > 0) {
                listeners.splice(index, 1);
            }
        } while (index > 0);
    };
};
