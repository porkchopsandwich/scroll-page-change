import { ContentHeightGetter, IScrollPageChangeState, ViewportHeightGetter } from "./interfaces";

export const updateDimensionsFactory = (
    viewportHeightGetter: ViewportHeightGetter,
    contentHeightGetter: ContentHeightGetter,
    state: IScrollPageChangeState,
): (() => void) => {
    return (): void => {
        state.viewportHeight = viewportHeightGetter();
        state.contentHeight = contentHeightGetter();
        state.pageCount = Math.floor(state.contentHeight / state.viewportHeight);
    };
};
