import { CurrentOffsetGetter } from "./interfaces";

export const currentOffsetGetterFactory = (): CurrentOffsetGetter => {
    return (): number => {
        return window.pageYOffset;
    };
};
