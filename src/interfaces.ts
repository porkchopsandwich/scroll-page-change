export interface IOnScrollChangeEvent {
    contentHeight: number;
    viewportHeight: number;
    topOffset: number;
    bottomOffset: number;
    pageCount: number;
    topFraction: number;
    bottomFraction: number;
    middleFraction: number;
    currentPage: number;
    relativeProgress: number;
}

export interface IScrollPageChange {
    addScrollChangeListener(listener: ScrollChangeListener): ScrollChangeUnsubscriber;
    removeScrollChangeListener(listener: ScrollChangeListener): void;
    addPageChangeListener(listener: ScrollChangeListener): ScrollChangeUnsubscriber;
    removePageChangeListener(listener: ScrollChangeListener): void;
    getPageCount(): number;
    updateDimensions(): void;
    getState(): Readonly<IScrollPageChangeState>;
}

export interface IScrollPageChangeState {
    viewportHeight: number;
    contentHeight: number;
    pageCount: number;
    currentPage: number | undefined;
}

export type ScrollChangeListener = (event: IOnScrollChangeEvent) => void;

export type ScrollChangeUnsubscriber = () => void;

export type ContentHeightGetter = () => number;

export type CurrentOffsetGetter = () => number;

export type ViewportHeightGetter = () => number;

export type OnScroll = () => void;

export type ScrollChangeAddListener = (listener: ScrollChangeListener) => ScrollChangeUnsubscriber;

export type ScrollChangeRemoveListener = (listener: ScrollChangeListener) => void;
